﻿/// <reference path="chord-utility.js" />
/// <reference path="_namespace.js" />
/// <reference path="point.js" />
/// <reference path="metrics.js" />
/// <reference path="neck.js" />
/// <reference path="chord.js" />
/// <reference path="chords.js" />
Fretboard.ChordExplorer = function () {
	var svg = document.getElementById('svg');

	var chordNameInput = document.getElementById('chord-name-input');
	var chordSymbol = document.getElementById('chord-symbol');
	var positionIndexCountLabel = document.getElementById('position-count');
	var positionIndexerNextButton = document.getElementById('position-indexer-next');
	var positionIndexerPreviousButton = document.getElementById('position-indexer-previous');
	var showFingeringInput = document.getElementById('show-fingering-input');
	var title = document.getElementById('title');
	var status = document.getElementById('status');

	var chordFamily = [];
	var selectedPosition = 0;

	chordNameInput.addEventListener('change', OnChordChanged);
	showFingeringInput.addEventListener('change', OnShowFingeringChanged);
	positionIndexerNextButton.addEventListener('click', OnNextPosition);
	positionIndexerPreviousButton.addEventListener('click', OnPreviousPosition);
	title.addEventListener('click', OnTitleClick);
	window.addEventListener('popstate', OnGoBack);
	window.addEventListener('resize', OnResize);

	var app = svg.parentNode;

	function Initialize(name) {
		chordNameInput.value = name;

		Fretboard.Neck.Draw(svg);
		OnChordChanged(null);
	}
	function Draw() {
		Fretboard.Neck.EraseFingerings();
		for (var i = 0; i < chordFamily.length; i++) {
			if (i == selectedPosition) {
				continue;
			}
			DrawDots(chordFamily[i], true);
		}
		DrawDots(chordFamily[selectedPosition], false);
		positionIndexCountLabel.textContent = selectedPosition + 1 + ' of ' + chordFamily.length;
	}
	function DrawDots(chord, ghost) {
		///<summary>Draw the fingered chored. If chost is true then fingered positions will be partially transparent.</summary>
		///<param name="finger" type="Chord"></param>	
		try {
			chord.Fingering.forEach(function (a) {
				FingeredDot(a, ghost);
			});
			OnShowFingeringChanged(null);
			status.textContent = 'Ready';
		}
		catch (e) {
			status.textContent = e;
		}
	}
	function FingeredDot(finger, ghost) {
		///<summary>Place a circle representing a fingered position on the fretboard.</summary>
		///<param name="finger" type="Finger"></param>		
		var dot = Fretboard.Neck.AddDot(finger, ghost);
		dot.addEventListener('click', OnSelectedFingeringChanged);
	}
	function OnChordChanged(e) {
		selectedPosition = 0;
		if (!chordNameInput.value) {
			return;
		}
		try {
			SetName(Fretboard.Chords.Utility.GetCanonicalName(chordNameInput.value));
			chordFamily = Fretboard.Chords.Utility.GetChordPatterns(chordNameInput.value);
			Draw();
		} catch (e) {
			status.textContent = e;
		}
	}
	function OnGoBack(e) {
		//History.log('History.stateChange: ' + $.param(state.data) + ' ' + state.url, event);
		// check state object here and control the display of articles or UI elements
		// eg if state.data.page === 1 then hide all pages except for page 1

		var state = e.state;

		Fretboard.Neck.EraseFingerings();
		Initialize(Fretboard.GetQueryParameterByName('chord'));

		e.preventDefault();
		return false;
	}
	function OnNextPosition(e) {
		selectedPosition = ++selectedPosition % chordFamily.length;
		Draw();
	}
	function OnPreviousPosition(e) {
		selectedPosition += chordFamily.length;
		selectedPosition = --selectedPosition % chordFamily.length;
		Draw();
	}
	function OnResize(e) {
		if (Fretboard.Metrics.Width == Math.round(.9 * app.clientWidth)) {
			return;
		}

		Fretboard.Neck.EraseFingerings();
		Fretboard.Neck.EraseFretboard();
		Initialize(Fretboard.GetQueryParameterByName('chord'));
	}
	function OnSelectedFingeringChanged(e) {
		var element = e.srcElement;
	}
	function OnShowFingeringChanged(e) {
		Fretboard.Neck.ShowFingering(showFingeringInput.checked ? 'visible' : 'hidden');
	}
	function OnTitleClick(e) {
		document.location.href = 'http://neocortex.com.au/Prototypes/Fretboard';
	}
	function SetName(name) {
		chordNameInput.value = name;
		chordSymbol.textContent = name;

		var query = '?chord=' + name;

		if (history.pushState) {
			var url = window.location.protocol + "//" + window.location.host + window.location.pathname + query;
			window.history.pushState({ path: url, query: query, name: name }, '', url);
		}
	}
	var self = {
		Initialize: Initialize
	};

	return self;
}();

var query = Fretboard.GetQueryParameterByName('chord') || 'C';

Fretboard.ChordExplorer.Initialize(query);