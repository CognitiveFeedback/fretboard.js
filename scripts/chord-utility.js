﻿/// <reference path="metrics.js" />
/// <reference path="chord.js" />
/// <reference path="chords.js" />
Fretboard.Chords.Utility = function () {
	var stem = /^([a-gA-G])([b#]{0,1})\s*(.*)/;

	// TRIADS
	var majorTriad = /^$/;
	var minorTriad = /^(m|min|Min)$/;
	var diminishedTriad = /^(dim|DIM|o|º)$/;
	var augmentedTriad = /^(aug|AUG)$/;

	// SUSPENDED 4TH
	var suspended4th = /^(sus[4]{0,1}|SUS[4]{0,1})$/;

	// 6th CHORDS
	var triad6th = /^(6)$/;

	// 7TH CHORDS
	var Major7th = /^(M7|Maj7|MAJ7|Δ)$/;
	var Minor7th = /^(m7|min7|Min7|MIN7)$/;
	var Dominant7th = /^(7)$/;
	var Diminished7th = /^(dim7|DIM7|o7|º7)$/;
	var Minor7b5 = /^(m7b5|Ø)$/;
	var MinorMajor7 = /^(mM7|minM7|minMaj7|minMAJ7|mMaj7|mMAJ7)$/;

	// ALTERED
	var Dominant7thSharp5 = /^(7#5)$/;
	var Dominant7thb5 = /^(7b5)$/;
	var Dominant7thSharp9 = /^(7#9)$/;
	var Dominant7thFlat9 = /^(7b9)$/;
	var Dominant7thSharp9Flat5 = /^7(#9b5|b5#9)$/;
	var Dominant7thSharp9Sharp5 = /^7(#9#5|#5#9)$/;
	var Dominant7thFlat9Flat5 = /^7(b9b5|b5b9)$/;
	var Dominant7thFlat9Sharp5 = /^7(b9#5|#5b9)$/;

	// 9th CHORDS
	var Dominant9th = /^(9)$/;
	var Minor9th = /^(m9|min9|Min9)$/;

	// 13TH CHORDS
	var Dominant13th = /^(13)$/;

	function GetChordFamily(name) {
		var result = [];
		var chord = {};
		var match = false;

		var type = name.match(stem);

		name = type[3];

		if (match = majorTriad.test(name)) { Fretboard.Chords.MajorTriad.forEach(function (a) { result.push(a); }); }
		else if (match = minorTriad.test(name)) { Fretboard.Chords.MinorTriad.forEach(function (a) { result.push(a); }); }
		else if (match = diminishedTriad.test(name)) { Fretboard.Chords.DiminishedTriad.forEach(function (a) { result.push(a); }); }
		else if (match = augmentedTriad.test(name)) { Fretboard.Chords.AugmentedTriad.forEach(function (a) { result.push(a); }); }
		else if (match = suspended4th.test(name)) { Fretboard.Chords.Suspended4thTriads.forEach(function (a) { result.push(a); }); }
		else if (match = triad6th.test(name)) { Fretboard.Chords.Triad6th.forEach(function (a) { result.push(a); }); }
		else if (match = Major7th.test(name)) { Fretboard.Chords.Major7th.forEach(function (a) { result.push(a); }); }
		else if (match = Minor7th.test(name)) { Fretboard.Chords.Minor7th.forEach(function (a) { result.push(a); }); }
		else if (match = Dominant7th.test(name)) { Fretboard.Chords.Dominant7th.forEach(function (a) { result.push(a); }); }
		else if (match = Diminished7th.test(name)) { Fretboard.Chords.Diminished7th.forEach(function (a) { result.push(a); }); }
		else if (match = Minor7b5.test(name)) { Fretboard.Chords.Minor7b5.forEach(function (a) { result.push(a); }); }
		else if (match = MinorMajor7.test(name)) { Fretboard.Chords.MinorMajor7.forEach(function (a) { result.push(a); }); }
		else if (match = Dominant7thSharp5.test(name)) { Fretboard.Chords.Dominant7th.forEach(function (a) { chord = a.Sharpen5th(); result.push(chord); }); }
		else if (match = Dominant7thb5.test(name)) { Fretboard.Chords.Dominant7th.forEach(function (a) { chord = a.Flaten5th(); result.push(chord); }); }
		else if (match = Dominant7thSharp9.test(name)) { Fretboard.Chords.Dominant9th.forEach(function (a) { chord = a.Sharpen9th(); result.push(chord); }); }
		else if (match = Dominant7thFlat9.test(name)) { Fretboard.Chords.Dominant9th.forEach(function (a) { chord = a.Flaten9th(); result.push(chord); }); }
		else if (match = Dominant7thSharp9Flat5.test(name)) { Fretboard.Chords.Dominant9th.forEach(function (a) { chord = a.Sharpen9th().Flaten5th(); result.push(chord); }); }
		else if (match = Dominant7thSharp9Sharp5.test(name)) { Fretboard.Chords.Dominant9th.forEach(function (a) { chord = a.Sharpen9th().Sharpen5th(); result.push(chord); }); }
		else if (match = Dominant7thFlat9Flat5.test(name)) { Fretboard.Chords.Dominant9th.forEach(function (a) { chord = a.Flaten9th().Flaten5th(); result.push(chord); }); }
		else if (match = Dominant7thFlat9Sharp5.test(name)) { Fretboard.Chords.Dominant9th.forEach(function (a) { chord = a.Flaten9th().Sharpen5th(); result.push(chord); }); }
		else if (match = Dominant9th.test(name)) { Fretboard.Chords.Dominant9th.forEach(function (a) { result.push(a); }); }
		else if (match = Dominant13th.test(name)) { Fretboard.Chords.Dominant13th.forEach(function (a) { result.push(a); }); }
		else if (match = Minor9th.test(name)) { Fretboard.Chords.Dominant9th.forEach(function (a) { chord = a.Flaten3rd(); result.push(chord); }); }

		return result;
	}

	function GetChordPatterns(name) {
		var result = [];
		var family = GetChordFamily(name);

		var groups = name.match(stem);
		name = groups[1].toUpperCase();
		if (groups.length >= 3) {
			name += groups[2];
		}

		family.forEach(function (a) {
			var diff = Fretboard.Notes.GetDistance(a.Name, name);

			chord = a.Transpose(diff);

			if (chord.GetLowestFingering() < 0) {
				chord = chord.Transpose(12);
			}

			if (chord.GetHighestFingering() > Fretboard.Metrics.HighestFret) {
				chord = chord.Transpose(-12);

				if (chord.GetLowestFingering() < 0) {
					return;
				}
			}

			result.push(chord);
		});
		return result;
	}

	function GetCanonicalName(value) {
		var groups = value.match(stem);
		var name = groups[1].toUpperCase();
		if (groups.length >= 3) {
			name += groups[2];
		}
		if (groups.length >= 4) {
			name += groups[3];
		}
		return name
	}

	var self = {
		GetCanonicalName: GetCanonicalName,
		GetChordPatterns: GetChordPatterns,
	};

	return self;
}();