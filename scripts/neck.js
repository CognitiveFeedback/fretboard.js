﻿/// <reference path="_namespace.js" />
/// <reference path="metrics.js" />
/// <reference path="chord.js" />
Fretboard.Neck = function () {
	var app = {};
	var fretboardLayer = {};
	var fingeringLayer = {};

	var width = 0;
	var height = 0;

	fretboardLayer = svg.getElementById('fretboard-layer');
	fingeringLayer = svg.getElementById('fingering-layer');

	var fingeringText = [];

	function AddDot(finger, ghost) {
		///<summary>Place a circle representing a fingered position on the fretboardLayer.</summary>
		///<param name="finger" type="Finger"></param>
		var dot = document.createElementNS(Fretboard.NS, "circle");
		dot.setAttributeNS(null, "cx", Fretboard.Metrics.FingerPosition(finger.Fret));
		dot.setAttributeNS(null, "cy", Fretboard.Metrics.StringPosition(finger.String));
		dot.setAttributeNS(null, "r", .017 * width);

		if (ghost) {
			dot.setAttributeNS(null, "fill", "white");
			dot.setAttributeNS(null, "opacity", ".1");
		}
		else {
			dot.setAttributeNS(null, "fill", "url(#fingering-dot-gradient)");
		}

		if (finger.Fret == 0) {
			dot.setAttributeNS(null, "cx", .011 * width + width * 0.004);
			dot.setAttributeNS(null, "r", .008 * width);
			dot.setAttributeNS(null, "fill", "transparent");
			dot.style.stroke = '#0090ff';
			dot.style.strokeWidth = width * 0.004;
		}

		fingeringLayer.appendChild(dot);

		if (!ghost && !!finger.Fret && !!finger.Finger) {
			var text = document.createElementNS(Fretboard.NS, "text");
			text.setAttribute('x', Fretboard.Metrics.FingerPosition(finger.Fret) - width * 0.006);
			text.setAttribute('y', Fretboard.Metrics.StringPosition(finger.String) + width * 0.007);
			text.textContent = finger.Finger;
			text.setAttributeNS(null, "fill", "white");
			text.style.fontSize = width * .0225 + 'px';
			text.style.fontWeight = 'lighter';
			fingeringLayer.appendChild(text);
			fingeringText.push(text);
		}

		return dot;
	}
	function AddFrets() {
		for (var i = 1; i <= Fretboard.Metrics.HighestFret; i++) {
			var position = Fretboard.Metrics.FretPosition(i);
			DrawFret(position, 2, position, height - 2);
		}
	}
	function AddNeckDetail(n) {
		var secondFret = Fretboard.Metrics.FretPosition(n - 1);
		var thirdfret = Fretboard.Metrics.FretPosition(n);
		var top = Fretboard.Metrics.StringPosition(2);
		var bottom = Fretboard.Metrics.StringPosition(5);

		var marginleft = width * 0.0125;
		var paddingtop = height * 0.131578947;
		if (n == 7 || n == 12) {
			r1 = {
				x: secondFret + marginleft,
				y: top - paddingtop,
				w: thirdfret - secondFret - marginleft * 2,
				h: (bottom - top + paddingtop * 2) / 2 - 10
			}

			r2 = {
				x: secondFret + marginleft,
				y: top - paddingtop + r1.h + 20,
				w: thirdfret - secondFret - marginleft * 2,
				h: (bottom - top + paddingtop * 2) / 2 - 10
			}

			DrawInlay(r1);
			DrawInlay(r2);
		}
		else {
			var r =
			{
				x: secondFret + marginleft,
				y: top - paddingtop,
				w: thirdfret - secondFret - marginleft * 2,
				h: bottom - top + paddingtop * 2
			};

			DrawInlay(r);
		}
	}
	function AddStrings() {
		for (var i = 1; i < 7; i++) {
			var position = Fretboard.Metrics.StringPosition(i);
			DrawString(Fretboard.Metrics.BarPosition, position, width, position, Fretboard.Metrics.StringGague(i));
		}
	}
	function DrawFretboard(svg) {
		app = svg.parentNode;

		Fretboard.Metrics.Calibrate(svg);

		width = svg.width.baseVal.value;
		height = svg.height.baseVal.value;

		Fretboard.Metrics.BarPosition = .04 * width;
		Fretboard.Metrics.BarWidth = .015 * width;

		DrawNeck();

		AddNeckDetail(3);
		AddNeckDetail(5);
		AddNeckDetail(7);
		AddNeckDetail(9);
		AddNeckDetail(12);

		AddFrets();
		AddStrings();
		DrawNut();
	}
	function DrawFret(x1, y1, x2, y2) {
		var shape = document.createElementNS(Fretboard.NS, "line");
		shape.x1.baseVal.value = x1;
		shape.x2.baseVal.value = x2;
		shape.y1.baseVal.value = y1;
		shape.y2.baseVal.value = y2;
		shape.style.stroke = 'white';
		shape.style.strokeWidth = width * 0.0035;
		fretboardLayer.appendChild(shape);
		return shape;
	}
	function DrawInlay(r) {
		var shape = document.createElementNS(Fretboard.NS, "rect");
		shape.x.baseVal.value = r.x;
		shape.y.baseVal.value = r.y;
		shape.width.baseVal.value = r.w;
		shape.height.baseVal.value = r.h;
		shape.setAttribute("height", r.h);
		shape.style.fill = 'url(#pearl-inlay)';
		shape.style.stroke = 'black';
		shape.style.strokeWidth = .5;
		fretboardLayer.appendChild(shape);
		return shape;
	}
	function DrawNeck() {
		var shape = document.createElementNS(Fretboard.NS, "rect");
		shape.x.baseVal.value = Fretboard.Metrics.BarPosition;
		shape.y.baseVal.value = 3;
		shape.width.baseVal.value = width;
		shape.height.baseVal.value = height - 6;
		shape.setAttribute("height", height - 6);
		shape.style.stroke = 'black';
		shape.style.strokeWidth = 2;
		shape.style.fill = 'url(#fretboardLayer-gradient)';
		fretboardLayer.appendChild(shape);
		return shape;
	}
	function DrawNut() {
		var shape = document.createElementNS(Fretboard.NS, "rect");
		shape.x.baseVal.value = 1;
		shape.y.baseVal.value = 1;
		shape.width.baseVal.value = Fretboard.Metrics.BarPosition;
		shape.height.baseVal.value = height - 2;
		shape.style.fill = 'url(#head-gradient)';
		shape.style.stroke = 'black';
		shape.style.strokeWidth = 1;
		fretboardLayer.appendChild(shape);
		return shape;
	}
	function DrawString(x1, y1, x2, y2, guage) {
		var shape = document.createElementNS(Fretboard.NS, "line");
		shape.x1.baseVal.value = x1;
		shape.x2.baseVal.value = x2;
		shape.y1.baseVal.value = y1;
		shape.y2.baseVal.value = y2;
		shape.style.stroke = '#cbcbcb';
		shape.style.strokeWidth = guage;
		fretboardLayer.appendChild(shape);
		return shape;
	}
	function EraseFingerings() {
		fingeringLayer.textContent = '';
	}
	function EraseFretboard() {
		fretboardLayer.textContent = '';
		fingeringText = [];
	}
	function ShowFingering(visibility) {
		fingeringText.forEach(function (a) { a.style.visibility = visibility; });
	}

	var self = {
		AddDot: AddDot,
		EraseFretboard: EraseFretboard,
		Draw: DrawFretboard,
		EraseFingerings: EraseFingerings,
		ShowFingering: ShowFingering
	};

	return self;
}();