﻿/// <reference path="chord.js" />
Fretboard.Chords = {};

// TRIADS
Fretboard.Chords.MajorTriad = function () {
	var self = [
	new Chord('C',
			[
				new Finger(3, 5, 4, 1),
				new Finger(2, 4, 3, 3),
				new Finger(0, 3, 1, 5),
				new Finger(1, 2, 2, 1),
				new Finger(0, 1, 1, 3),
			]),
	new Chord('A',
			[
				new Finger(0, 6, 1, 5),
				new Finger(0, 5, 1, 1),
				new Finger(2, 4, 3, 5),
				new Finger(2, 3, 3, 1),
				new Finger(2, 2, 3, 3),
				new Finger(0, 1, 1, 5),
			]),
	new Chord('G',
			[
				new Finger(3, 6, 3, 1),
				new Finger(2, 5, 2, 3),
				new Finger(0, 4, 1, 5),
				new Finger(0, 3, 1, 1),
				new Finger(0, 2, 1, 3),
				new Finger(3, 1, 4, 1),
			]),
	new Chord('E',
			[
				new Finger(0, 6, 1, 1),
				new Finger(2, 5, 3, 5),
				new Finger(2, 4, 4, 1),
				new Finger(1, 3, 2, 3),
				new Finger(0, 2, 1, 5),
				new Finger(0, 1, 1, 1),
			])
	];

	self[3].OpenOnly = true;

	return self;
}();

Fretboard.Chords.MinorTriad = function () {
	return [
	new Chord('Em',
			[
				new Finger(0, 6, 1),
				new Finger(2, 5, 3),
				new Finger(2, 4, 4),
				new Finger(0, 3, 1),
				new Finger(0, 2, 1),
				new Finger(0, 1, 1),
			]),
	new Chord('Am',
			[
				new Finger(0, 6, 1),
				new Finger(0, 5, 1),
				new Finger(2, 4, 3),
				new Finger(2, 3, 4),
				new Finger(1, 2, 2),
				new Finger(0, 1, 1),
			])
	];
}();

Fretboard.Chords.DiminishedTriad = function () {
	return [
	new Chord('Eb',
			[
				new Finger(1, 5, 1),
				new Finger(2, 4, 2),
				new Finger(3, 3, 4),
				new Finger(2, 2, 3),
			]),
	];
}();

Fretboard.Chords.AugmentedTriad = function () {
	return [
	new Chord('D',
			[
				new Finger(5, 5, 3),
				new Finger(4, 4, 2),
				new Finger(3, 3, 1),
			]),
	];
}();

Fretboard.Chords.Suspended4thTriads = function () {
	var self = [
	new Chord('Asus4',
			[
				new Finger(0, 6, 1, 5),
				new Finger(0, 5, 1, 1),
				new Finger(2, 4, 2, 5),
				new Finger(2, 3, 3, 1),
				new Finger(3, 2, 4, 4),
				new Finger(0, 1, 1, 5),
			]),
	new Chord('Dsus4',
			[
				new Finger(0, 4, 1, 1),
				new Finger(2, 3, 2, 5),
				new Finger(3, 2, 3, 1),
				new Finger(3, 1, 4, 4),
			]),
	new Chord('Esus4',
			[
				new Finger(0, 6, 1, 1),
				new Finger(2, 5, 2, 5),
				new Finger(2, 4, 3, 1),
				new Finger(2, 3, 4, 3),
				new Finger(0, 2, 1, 5),
				new Finger(0, 1, 1, 1),
			])
	];
	return self
}();

Fretboard.Chords.Triad6th = function () {
	var self = [
	new Chord('E6',
			[
				new Finger(7, 5, 4, 1),
				new Finger(6, 4, 3, 3),
				new Finger(6, 3, 2, 6),
				new Finger(5, 2, 1, 1),
			]),
	new Chord('A6',
			[
				new Finger(0, 6, 1, 5),
				new Finger(0, 5, 1, 1),
				new Finger(2, 4, 3, 5),
				new Finger(2, 3, 3, 1),
				new Finger(2, 2, 3, 3),
				new Finger(2, 1, 1, 6),
			])

	];
	return self
}();

// 7th CHORDS
Fretboard.Chords.Major7th = function () {
	return [
	new Chord('AM7',
			[
				new Finger(5, 6, 1),
				new Finger(5, 2, 2),
				new Finger(6, 3, 3),
				new Finger(6, 4, 4),
			]),
	new Chord('CM7',
			[
				new Finger(3, 5, 1),
				new Finger(5, 4, 3),
				new Finger(4, 3, 2),
				new Finger(5, 2, 4),
			]),
	new Chord('EM7',
			[
				new Finger(2, 4, 1),
				new Finger(4, 3, 3),
				new Finger(4, 2, 3),
				new Finger(4, 1, 3),
			]),
	new Chord('EM7',
			[
				new Finger(7, 5, 4, 1),
				new Finger(6, 4, 3, 3),
				new Finger(4, 3, 1, 5),
				new Finger(4, 2, 3, 7),
			])
	];
}();

Fretboard.Chords.Minor7th = function () {
	return [
	new Chord('Am7',
			[
				new Finger(5, 6, 1),
				new Finger(7, 5, 3),
				new Finger(5, 4, 1),
				new Finger(5, 3, 1),
				new Finger(5, 2, 1),
				new Finger(5, 1, 1),
			]),
	new Chord('Cm7',
			[
				new Finger(3, 5, 1),
				new Finger(5, 4, 4),
				new Finger(3, 3, 2),
				new Finger(4, 2, 3),
			]),
	new Chord('Cm7',
			[
				new Finger(3, 5, 1),
				new Finger(3, 3, 2),
				new Finger(4, 2, 4),
				new Finger(3, 1, 3),
			]),
	new Chord('Em7',
			[
				new Finger(2, 4, 1),
				new Finger(4, 3, 4),
				new Finger(3, 2, 3),
				new Finger(3, 1, 2),
			])
	];
}();

Fretboard.Chords.Dominant7th = function () {
	return [
	new Chord('A7',
			[
				new Finger(5, 6, 1, 1),
				new Finger(5, 4, 2, 7),
				new Finger(6, 3, 4, 3),
				new Finger(5, 2, 3, 5),
			]),
	new Chord('C7',
			[
				new Finger(3, 5, 3, 1),
				new Finger(2, 4, 2, 3),
				new Finger(3, 3, 4, 7),
				new Finger(1, 2, 1, 1),
			]),
	new Chord('C7',
			[
				new Finger(3, 5, 1, 1),
				new Finger(5, 4, 3, 5),
				new Finger(3, 3, 1, 7),
				new Finger(5, 2, 4, 3),
			], 1),
	new Chord('F7',
			[
				new Finger(3, 4, 1, 1),
				new Finger(5, 3, 2, 5),
				new Finger(4, 2, 4, 7),
				new Finger(5, 1, 3, 3),
			])
	];
}();

Fretboard.Chords.Diminished7th = function () {
	return [
	new Chord('Adim7',
			[
				new Finger(5, 6, 3),
				new Finger(4, 4, 1),
				new Finger(5, 3, 2),
				new Finger(4, 2, 4),
			]),
	new Chord('Adim7',
			[
				new Finger(7, 4, 1),
				new Finger(8, 3, 3),
				new Finger(7, 2, 2),
				new Finger(8, 1, 4),
			]),
	new Chord('Cdim7',
			[
				new Finger(3, 5, 2),
				new Finger(4, 4, 3),
				new Finger(2, 3, 1),
				new Finger(4, 2, 4),
			])
	];
}();

Fretboard.Chords.Minor7b5 = function () {
	return [
	new Chord('AØ',
			[
				new Finger(5, 6, 2),
				new Finger(5, 4, 3),
				new Finger(5, 3, 4),
				new Finger(4, 2, 1),
			]),
	new Chord('EØ',
			[
				new Finger(2, 4, 1),
				new Finger(3, 3, 2),
				new Finger(3, 2, 2),
				new Finger(3, 1, 2),
			]),
	new Chord('CØ',
			[
				new Finger(3, 5, 1),
				new Finger(4, 4, 3),
				new Finger(3, 3, 2),
				new Finger(4, 2, 4),
			])
	];
}();

Fretboard.Chords.MinorMajor7 = function () {
	return [
	new Chord('AmM7',
			[
				new Finger(5, 6, 1),
				new Finger(6, 4, 2),
				new Finger(5, 3, 3),
				new Finger(5, 2, 4),
			]),
	new Chord('CmM7',
			[
				new Finger(3, 5, 1),
				new Finger(5, 4, 4),
				new Finger(4, 3, 3),
				new Finger(4, 2, 2),
			]),
	new Chord('EmM7',
			[
				new Finger(2, 4, 1),
				new Finger(4, 3, 4),
				new Finger(4, 2, 3),
				new Finger(3, 1, 2),
			])
	];
}();

// 9th CHORDS
Fretboard.Chords.Dominant9th = function () {
	return [
	new Chord('A9',
			[
				new Finger(5, 6, 1, 1),
				new Finger(5, 4, 1, 7),
				new Finger(6, 3, 2, 3),
				new Finger(7, 1, 4, 9),
			]),
	new Chord('E9',
			[
				new Finger(7, 5, 2, 1),
				new Finger(6, 4, 1, 3),
				new Finger(7, 3, 3, 7),
				new Finger(7, 2, 3, 9),
				new Finger(7, 1, 3, 5),
			])
	];
}();

// 13th CHORDS
Fretboard.Chords.Dominant13th = function () {
	return [
	new Chord('A13',
			[
				new Finger(5, 6, 1, 1),
				new Finger(5, 4, 2, 7),
				new Finger(6, 3, 3, 3),
				new Finger(7, 2, 4, 13),
			]),
	new Chord('E13',
			[
				new Finger(7, 5, 2, 1),
				new Finger(6, 4, 1, 3),
				new Finger(7, 3, 3, 7),
				new Finger(7, 2, 3, 9),
				new Finger(9, 1, 4, 13),
			])
	];
}();