﻿function Finger(fret, string, finger, degree) {
	var Finger = finger || 0;
	var Fret = fret;
	var String = string;
	var Degree = degree || 0;
	var OpenOnly = false;

	var self = {
		Degree: Degree,
		Finger: Finger,
		Fret: Fret,
		String: String,
		OpenOnly: OpenOnly
	};

	return self;
}

function Chord(name, fingering, difficulty) {
	var Name = name;
	var Inversion = 1;
	var Fingering = fingering;
	var Difficulty = difficulty || 0;

	function Copy() {
		var copy = JSON.parse(JSON.stringify(this));
		var result = new Chord(copy.Name, copy.Fingering);
		return result;
	}
	function Transpose(n) {
		var result = self.Copy();
		result.Fingering.forEach(function (a) {
			a.Fret += n;
		});
		return result;
	}
	function GetLowestFingering() {
		var result = 100;
		this.Fingering.forEach(function (a) {
			if (a.Fret < result) {
				result = a.Fret;
			}
		});
		return result;
	}
	function GetHighestFingering() {
		var result = -1;
		this.Fingering.forEach(function (a) {
			if (a.Fret > result) {
				result = a.Fret;
			}
		});
		return result;
	}
	function Sharpen5th() {
		var result = self.Copy();
		var altered = false;
		result.Fingering.forEach(function (a) {
			if (a.Degree == 5) {
				a.Fret++;
				altered = true;
			}
		});
		if (altered) { result.Fingering.forEach(function (a) { a.Finger = 0; }); }
		return result;
	}
	function Flaten5th() {
		var result = self.Copy();
		var altered = false;
		result.Fingering.forEach(function (a) {
			if (a.Degree == 5) {
				a.Fret--;
				altered = true;
			}
		});
		if (altered) { result.Fingering.forEach(function (a) { a.Finger = 0; }); }
		return result;
	}
	function Sharpen9th() {
		var result = self.Copy();
		var altered = false;
		result.Fingering.forEach(function (a) {
			if (a.Degree == 9) {
				a.Fret++;
				altered = true;
			}
		});
		if (altered) { result.Fingering.forEach(function (a) { a.Finger = 0; }); }
		return result;
	}
	function Flaten9th() {
		var result = self.Copy();
		var altered = false;
		result.Fingering.forEach(function (a) {
			if (a.Degree == 9) {
				a.Fret--;
				altered = true;
			}
		});
		if (altered) { result.Fingering.forEach(function (a) { a.Finger = 0; }); }
		return result;
	}
	function Flaten3rd() {
		var result = self.Copy();
		result.Fingering.forEach(function (a) {
			if (a.Degree == 3) {
				a.Fret--;
			}
		});
		return result;
	}
	function Sharpen3rd() {
		var result = self.Copy();
		result.Fingering.forEach(function (a) {
			if (a.Degree == 3) {
				a.Fret++;
			}
		});
		return result;
	}
	function IsOpen() {
		var result = false;
		result.Fingering.forEach(function (a) {
			if (a.Finger == 0) {
				result = true;
				return;
			}
		});
		return result;
	}

	var self = {
		Copy: Copy,
		Fingering: Fingering,
		Flaten9th: Flaten9th,
		Flaten5th: Flaten5th,
		Flaten3rd: Flaten3rd,
		GetLowestFingering: GetLowestFingering,
		GetHighestFingering: GetHighestFingering,
		Inversion: Inversion,
		IsOpen: IsOpen,
		Name: Name,
		Sharpen3rd: Sharpen3rd,
		Sharpen9th: Sharpen9th,
		Sharpen5th: Sharpen5th,
		Transpose: Transpose,
	};

	return self;
}

Fretboard.Notes = function () {
	function GetDifference(a, b) {
		if (!/^([a-gA-G][b#]{0,1}).*$/.test(a) || !/^([a-gA-G][b#]{0,1}).*$/.test(b)) {
			throw "Argument out of range: notes must be standard enharmonically spelled notes.";
		}
		var result = 3;

		var aGroups = a.match(/^([a-gA-G][b#]{0,1}).*$/);
		var bGroups = b.match(/^([a-gA-G][b#]{0,1}).*$/);

		var a = aGroups[1].replace('#', 's');
		var b = bGroups[1].replace('#', 's');

		var numa = Fretboard.Notes[a];
		var numb = Fretboard.Notes[b];

		result = numb - numa;

		return result;
	}

	var self = {
		GetDistance: GetDifference,

		Ab: 12,
		A: 1,
		As: 2,

		Bb: 2,
		B: 3,
		Bs: 4,

		Cb: 3,
		C: 4,
		Cs: 5,

		Db: 5,
		D: 6,
		Ds: 7,

		Eb: 7,
		E: 8,
		Es: 9,

		Fb: 8,
		F: 9,
		Fs: 10,

		Gb: 10,
		G: 11,
		Gs: 12,
	};

	return self;
}();
