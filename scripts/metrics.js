﻿Fretboard.Metrics = function () {
	var width = 0;
	var height = 0;

	var highestFret = 12;
	var barPosition = 0;

	function Calibrate(svg) {
		var fretboardApp = svg.parentNode;

		width = Math.round(.9 * fretboardApp.clientWidth);
		height = Math.round(0.2275 * width);

		svg.setAttribute('width', width + "px");
		svg.setAttribute('height', height + "px");

		barPosition = width * 0.0375;
	}

	function FretPosition(n) {
		///<summary cref="http://liutaiomottola.com/formulae/fret.htm">Fret length spacing for n-th fret</summary>
		///<param name="n" type="Number">Fret number</param>
		var length = width * 1.85;
		var position = barPosition + length - (length / Math.pow(2, (n / 12)));
		return position;
	}

	function FingerPosition(n) {
		if (n < 0 || n > highestFret) {
			throw "Argument out of range: N-th Fret must be between 0 and 9 inclusive. n = " + n;
		}
		var p = FretPosition(n - 1);
		var q = FretPosition(n);

		var position = p + (q - p) / 2;

		return position;
	}

	function StringPosition(n) {
		if (n < 1 || n > 7) {
			throw "Argument out of range: N-th String must be between 1 and 6 inclusive. n = " + n;
		}
		var result = 0.086 * height + (n - 1) * 0.165 * height;
		return result;
	}

	function StringGague(n) {
		if (n < 1 || n > 6) {
			throw "Argument out of range: N-th String must be between 1 and 6 inclusive. n = " + n;
		}
		var result = 0.018 * height / 6 * (n - 1) + 1.5;
		return result;
	}

	var self = {
		BarPosition: barPosition,
		Calibrate: Calibrate,
		FingerPosition: FingerPosition,
		FretPosition: FretPosition,
		Height: height,
		HighestFret: highestFret,
		StringGague: StringGague,
		StringPosition: StringPosition,
		Width: width
	};

	return self;
}();